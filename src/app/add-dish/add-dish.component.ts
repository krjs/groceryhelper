import { HttpService } from './../services/http.service';
import { DishService } from './../services/dish.service';
import { Dish } from './../model/dish.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-dish',
  templateUrl: './add-dish.component.html',
  styleUrls: ['./add-dish.component.scss']
})
export class AddDishComponent implements OnInit {
  arr: Array<string>;
  arr2: Array<string>;
  addForm: FormGroup;
  dishRecipe: string;
  public sub;
  public checker;
  public modalInfo: Object = new Object();

  constructor(
    private cS: DishService,
    private http: HttpService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router) {
    this.arr = this.cS.dishcategory;
    this.arr2 = ['Biedronka', 'Lidl', 'Zabka', 'Inne'];
  }

  ngOnInit() {
    this.arr = this.cS.dishcategory;
    this.arr2 = ['Biedronka', 'Lidl', 'Zabka', 'Inne'];
    this.addForm = this.clearDishForm();

    this.sub = this.route.snapshot.queryParamMap.get('id');
    this.modalInfoText(this.sub)

    if (this.sub !== null) {
      this.editDish();
    }
  }

  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }

  modalInfoText(state) {
    this.modalInfo = {
      head: state ? "Danie zostało edytowane" : "Danie zostało dodane",
      mainInfo: "Zobacz danie w zakładce Lista Dań",
      closeButton: "Zamknij"
    }
  }

  addProductForDish() {
    const data = <FormArray>this.addForm.get('product');

    data.push(new FormGroup({
      produkt: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      sklep: new FormControl(this.arr2[3])
    }));

  }

  clearDishForm() {
    return new FormGroup({
      dishName: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      categoryName: new FormControl(this.arr[3], [Validators.required]),
      dishRecipe: new FormControl('Przepis'),
      product: new FormArray([new FormGroup({
        produkt: new FormControl(null, [Validators.required, Validators.minLength(3)]),
        sklep: new FormControl(this.arr2[3])
      })
      ], this.dishProductValidator)
    });
  }

  removeProductField(i) {
    let data = <FormArray>this.addForm.get('product');

    data.removeAt(i);
  }

  dishProductValidator(control: AbstractControl): ValidationErrors {
    const arrOfProduct = <[any]>control.value;
    let checker = false;

    arrOfProduct.map(item => {
      if (item.produkt == null || item.produkt == undefined || item.produkt.length < 2) {
        checker = true
      } else {
        checker = false
      }
    })

    if (checker) {
      return { 'empty': true }
    }
  }

  addDish() {
    const dish = this.createDishList();

    this.http.saveDish(dish);
    this.cS.add(dish)
    this.addForm = this.clearDishForm()
  }

  editDish() {
    let elem;
    this.sub = this.route.snapshot.queryParamMap.get('id');
    this.http.getDishById(this.sub).subscribe(data => {
      data.forEach(element => {
        elem = element;
      });

      this.setFormVale(elem);
    });
  }

  saveEditedDish() {
    this.sub = this.route.snapshot.queryParamMap.get('id');
    this.modalInfoText(this.sub)
    const dish = this.createDishList();

    this.http.saveDishWithById(this.sub, dish);
    this.cS.add(dish)
    this.addForm = this.clearDishForm();
  }

  setFormVale(dish) {
    this.setProductFormValue(dish.product)
    this.addForm.patchValue({
      dishName: dish.dish,
      categoryName: dish.category,
      dishRecipe: dish.dishRecipe,
      product: dish.product
    })
  }

  setProductFormValue(produce) {
    const data = <FormArray>this.addForm.get('product');

    for (let i = 1; i < produce.length; i++) {
      data.push(new FormGroup({
        produkt: new FormControl(),
        sklep: new FormControl()
      }));
    }
  }

  createDishList() {
    const dishToAdd = {
      dish: <string>this.addForm.get('dishName').value.toLowerCase().trim(),
      category: <string>this.addForm.get('categoryName').value,
      dishRecipe: <string>this.addForm.get('dishRecipe').value.toLowerCase(),
      product: new Array()
    },
      dishProductChild = this.addForm.get('product').value;
    dishProductChild.forEach(element => {
      const singleProduct = {
        produkt: element.produkt.toLowerCase().trim(),
        sklep: element.sklep
      }

      dishToAdd.product.push(singleProduct);
    });

    return dishToAdd;
  }

  onSubmit() {
    this.addDish();
  }
}

