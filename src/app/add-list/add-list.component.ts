import { Component, OnInit } from '@angular/core';
import { UserListService } from './../services/user-list.service';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { DishService } from '../services/dish.service';
import { HttpService } from '../services/http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.scss']
})
export class AddListComponent implements OnInit {

  public userListView;
  public allUserDish;
  userListChoosedDishId = new Array();
  userListChoosedDish = new Array();
  userGroceryFinder = new Array();
  userListForm: FormGroup;
  listId;
  public modalInfo: Object = new Object();

  constructor(
    public userList: UserListService,
    public dishService: DishService,
    private http: HttpService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router) {
    this.getAllUserDish();
    this.userListForm = new FormGroup({
      listName: new FormControl(''),
      userChoosedDish: new FormArray([])
    });
   }

  ngOnInit() {
    this.listId = this.route.snapshot.queryParamMap.get('id');
    this.getAllUserDish();

    this.modalInfoText(this.listId)
    if(this.listId !== null) {
      this.editList()
    } else {
      this.setUserDishList();
    }
  }

 clearForm() {
  this.userListChoosedDish = []
   return new FormGroup({
    listName: new FormControl(''),
    userChoosedDish: new FormArray([])
  });
 }

  onSubmit() {
    this.saveUserList();
    this.userListForm = this.clearForm();
  }

  getAllUserDish() {
    this.dishService.getDishList().subscribe(list => {
      this.allUserDish = list;
    })
  }

  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }

  modalInfoText(state) {
    this.modalInfo = {
      head: state ? "Lista zostało edytowane" : "Lista zostało dodane",
      mainInfo: "Zobacz liste w zakładce Lista Dań",
      closeButton: "Zamknij"
    }
  }

  editList() {
    this.listId = this.route.snapshot.queryParamMap.get('id');
    this.userListChoosedDish = [];
    this.userList.getUserListBeId(this.listId).subscribe(data =>{

      data.map(item =>{
        this.userListForm.patchValue({
          listName: item.listName
        })

        item.userChoosedDish.map(item=>{
          this.addToSpecifedList(item._id.$oid)
        })
      })
      })
  }

  saveEditedList() {
    let userList = {
      listName: <string>this.userListForm.get('listName').value,
      userChoosedDish: this.userListChoosedDish
    }
    this.modalInfoText(this.listId)
    if(userList.userChoosedDish.length !== 0) {
      this.http.saveListById(this.listId, userList)
    }

    this.userListForm = this.clearForm();
  }

  removeSelectedDishFromList(id) {
    this.setRemovedDishToUserList(id)

    this.userListChoosedDish = this.userListChoosedDish.filter(item => item._id.$oid !== id)
  }

  setRemovedDishToUserList(id) {
    this.http.getDishById(id).subscribe(data => {
      data.forEach(element => {
        this.allUserDish.push(element)
      });
    })

    this.addToSpecifedList(id)
  }

  removeSelectedDishFromUserList(id) {
    this.allUserDish = this.allUserDish.filter(item => item._id.$oid !== id)
  }

  saveUserList() {
    let userData = {
      listName: <string>this.userListForm.get('listName').value,
      userChoosedDish: this.userListChoosedDish
    }

    if(userData.userChoosedDish.length !== 0) {
      this.http.saveUserList(userData);
    }
  }

  setUserDishList() {
    this.userList.getUserList().subscribe(data => {

      this.userListView = data;
    })
  }

  addToSpecifedList(id: string) {
    if (!this.userListChoosedDishId.includes(id)) {
      this.userListChoosedDishId.push(id)
      this.getDishBeId(this.userListChoosedDishId)
    } else {
      this.userListChoosedDishId = this.userListChoosedDishId.filter(item => item !== id)
    }
  }

  getDishBeId(ArrID) {
    this.userListChoosedDish = [];
    let dishByID =[];

    ArrID.map(itemId =>{
      this.http.getDishById(itemId).subscribe(data => {
        data.forEach(element => {
          dishByID.push(element)
        });

        this.userListChoosedDish = dishByID;
      })
    })
  }
}
