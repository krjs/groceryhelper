import { HomeComponent } from './home/home.component';
import { AddDishComponent } from './add-dish/add-dish.component';
import { LoginComponent } from './auth/login/login.component';
import { DishCategoryComponent } from './dish-category/dish-category.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuardsService } from './auth/auth-guards.service';
import { UserListComponent } from './user-list/user-list.component';
import { AddListComponent } from './add-list/add-list.component';
import { LoggedUserGuardService } from './auth/logged-user-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'dish-list',
    component: DishCategoryComponent,
    canActivate: [AuthGuardsService]
  },
  {
    path: 'add-dish',
    component: AddDishComponent,
    canActivate: [AuthGuardsService]
  },
  {
    path: 'add-list',
    component: AddListComponent,
    canActivate: [AuthGuardsService]
  },
  {
    path: 'user-list',
    component: UserListComponent,
    canActivate: [AuthGuardsService]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoggedUserGuardService]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
