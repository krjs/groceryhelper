import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DishesListComponent } from './dishes-list/dishes-list.component';
import { DishCategoryComponent } from './dish-category/dish-category.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AddDishComponent } from './add-dish/add-dish.component';
import { LoginComponent } from './auth/login/login.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { config } from './auth/firebase.config';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpService } from './services/http.service';
import { HeadInterceptor } from './services/helpers/head.interceptor';
import { DishService } from './services/dish.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserListComponent } from './user-list/user-list.component';
import { AddListComponent } from './add-list/add-list.component';
import { TransformListPipe } from './visualHelper/transform-list.pipe';
import { DishCategoryDirective } from './visualHelper/dish-category.directive';
import { ShopsIconDirective } from './visualHelper/shops-icon.directive';
import { environment} from '../environments/environment';  // will be change later

@NgModule({
  declarations: [
    AppComponent,
    DishesListComponent,
    DishCategoryComponent,
    HeaderComponent,
    FooterComponent,
    AddDishComponent,
    LoginComponent,
    PageNotFoundComponent,
    HomeComponent,
    UserListComponent,
    AddListComponent,
    TransformListPipe,
    DishCategoryDirective,
    ShopsIconDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [HttpService, DishService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: HeadInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
