import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'firebase';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  errorMesage:any;
  user: User;
  private logInErrorSubject = new Subject<string>();

  constructor( private aF: AngularFireAuth, private router: Router) {

    this.aF.authState.subscribe(user => {
      this.user = user ;
    });
  }

  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
}

  login(email: string, password: string) {
    this.aF.auth.signInWithEmailAndPassword(email, password)
    .then(user => {
      this.router.navigate(['/add-dish']);
    }).catch(err => {
      this.logInErrorSubject.next(err.message);
    })
  }

  signup(email: string, password: string) {
    this.aF.auth.createUserWithEmailAndPassword(email, password)
    .then( user => {
      console.log(user)
      this.router.navigate(['/add-dish']);
    }).catch (err => {
      this.logInErrorSubject.next(err.message);
    })
  }

  logout() {
    this.aF.auth.signOut();
  }
}
