import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/interfaces';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';


@Injectable({
  providedIn: 'root'
})
export class LoggedUserGuardService implements CanActivate {
  user: any;
  constructor(public authService: AuthService, public router: Router, private aF: AngularFireAuth) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.aF.authState.subscribe(current => {
      this.checkUserState(current)
    });

    return this.user

  }

  checkUserState(state) {
    if(state){
      this.router.navigate(['/add-dish']);
      this.user = false;
    } else {
      this.user = true;
    }
  }
}
