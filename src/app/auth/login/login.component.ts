import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public errorMessage: string;

  constructor(private authService: AuthService) {
    this.authService.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
    });
  }

  ngOnInit() {
    this.authService.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
    });
  }

  login(formData: NgForm) {
    this.authService.login(formData.value.email, formData.value.password);
  }

  signup(formData) {
    this.authService.signup(formData.value.email, formData.value.password);
  }
}
