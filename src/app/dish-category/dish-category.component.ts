import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dish-category',
  templateUrl: './dish-category.component.html',
  styleUrls: ['./dish-category.component.scss']
})
export class DishCategoryComponent implements OnInit {

  constructor() { }

  dishCategory = ["Obiadowe", "Lekkie", "Desery", "Inne"];

  ngOnInit() {
  }

}
