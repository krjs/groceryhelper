import { HttpService } from "./../services/http.service";
import { Observable } from "rxjs";
import { Component, OnInit, Input } from "@angular/core";
import { DishService } from "../services/dish.service";
import { Dish } from "../model/dish.model";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-dishes-list",
  templateUrl: "./dishes-list.component.html",
  styleUrls: ["./dishes-list.component.scss"]
})
export class DishesListComponent implements OnInit {
  @Input()
  dishCategory;

  public dishesList;
  categoryCheck: boolean;

  constructor(
    public dishService: DishService,
    public http: HttpService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.setDishList();
  }

  ngOnInit() {
    this.setDishList();
  }

  setDishList() {
    this.dishService.getDishList().subscribe(list => {
      this.dishesList = list;
      this.isCategoryDishAvible();
    });
  }

  isCategoryDishAvible() {
    this.dishesList.map(item => {
      if (item.category === this.dishCategory) {
        this.categoryCheck = true;
      }
    });
  }

  deleteDish(dish: Dish) {
    this.http.deleteDish(dish._id.$oid);
  }

  editDish(dish: Dish) {
    let dishToEditId = {
      id: dish._id.$oid
    };

    this.router.navigate(["/add-dish"], { queryParams: dishToEditId });
  }
}
