

export interface Dish {
    _id?: {
      $oid: string
    };
    category: string;
    dish: string;
    dishRecipe?: string;
    deleted?: boolean;
    product: {
      produkt: string;
      sklep: string
    }[];
}
