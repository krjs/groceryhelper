export interface UserList {
  _id?: {
    $oid: string
  };
  deleted?:boolean
  name: string;
  dishId?: string;
}
