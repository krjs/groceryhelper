import { HttpService } from './http.service';
import { Dish } from './../model/dish.model';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class DishService {
  public dishListObs = new BehaviorSubject<Array<Dish>>([]);
  public dishList;
  dishcategory =  ["Obiadowe", "Lekkie", "Desery", "Inne"];

  constructor(private http: HttpService) {
   this.getDishList();

    this.dishListObs.next(this.dishList);
  }

  getDishList(): Observable<Array<Dish>> {
    return this.http.getDish();
  }

  add(dish: Dish) {
    const list = this.dishListObs.getValue();
     this.dishListObs.next(list);
  }

  saveDishInDb() {
    this.http.saveDish(this.dishListObs.getValue());
  }
}
