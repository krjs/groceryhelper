import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HeadInterceptor implements HttpInterceptor {
  readonly apiKey = 'F4qhZnkln4X2BddvxUtZ2s4ut4rTvcv2';

 intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const clonedReq = req.clone({
      params: req.params.set('apiKey', this.apiKey)
    })

    return next.handle(clonedReq);
 }
}
