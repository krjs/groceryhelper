import { Observable, BehaviorSubject } from 'rxjs';
import { Dish } from './../model/dish.model';
import { AuthService } from './../auth/auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UserList } from '../model/user-list.model';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private url = 'https://api.mlab.com/api/1/databases/grocery_helper/collections/';
  readonly apiKey = 'F4qhZnkln4X2BddvxUtZ2s4ut4rTvcv2';
  readonly params: HttpParams;
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  private addToReq = {
    headers: this.httpHeaders
  }

  constructor(private http: HttpClient, private aS: AuthService) {  }

  getDish(): Observable<any> {
    const db_url = this.createUrl();
    return this.http.get<Array<Dish>>(db_url);
  }

  getUserList(): Observable<any> {
    const db_url = this.createUrl('list');
    return this.http.get<any>(db_url);
  }

  getSelectedDish(dish) {
        const db_url = this.createUrl() + this.getParamSpecificDish(dish);

        return this.http.get<any>(db_url);
  }

  getDishById(id: string) {
    const db_url = this.createUrl() + this.getItemFromDbById(id);
    return this.http.get<any>(db_url);
  }

  getUserListById(id: string): Observable<any> {
    const db_url = this.createUrl('list') + this.getItemFromDbById(id);
    return this.http.get<any>(db_url);
  }

  createUrl(list: string = ''): string {
    let userId = this.aS.user.uid + list;

    if (this.aS.user === null) {
      userId = 'default';
    }

    return this.url + userId;
  }

  getItemFromDbById(id: string): HttpParams {
    const query = {
      '_id': {
        '$oid': id
      }
    };

    return new HttpParams()
    .append('?q', JSON.stringify(query))
  }

  getParamSpecificDish(dish): HttpParams {
    const query = {
      'category':  dish.category,
      'dish': dish.dish
    }
    return new HttpParams()
    .append('?q', JSON.stringify(query))
  }

  getParams(): HttpParams {
    return new HttpParams()
      .append('?apiKey', this.apiKey);
  }

  saveDish(list) {
    const db_url = this.createUrl();

    this.http.post<any>(db_url, JSON.stringify(list), this.addToReq).subscribe();
  }

  saveDishWithById(id, dish) {
    const db_url = this.createUrl() + this.getItemFromDbById(id);

    this.http.put<any>(db_url, JSON.stringify(dish), this.addToReq).subscribe();
  }

  deleteDish(dishId) {
    let dish:Dish;

    this.getDishById(dishId).subscribe(data =>{
      data.map(item => {
        dish = item;
        dish.deleted = true;
        this.saveDishWithById(dishId, dish)
      })
    })
  }

  deleteUserList(listID) {
    let userList:UserList;

    this.getUserListById(listID).subscribe(data =>{
          data.map(item =>{
            userList = item;
            userList.deleted = true;
            this.saveUserList(userList)
          })
      }
    )
  }

  saveUserList(userList) {
    const db_url = this.createUrl('list') + this.getParams();

    this.http.post<any>(db_url, JSON.stringify(userList), this.addToReq).subscribe();
  }

  saveListById(id, list) {
    const db_url = this.createUrl('list') + this.getItemFromDbById(id);

    this.http.put<any>(db_url, JSON.stringify(list), this.addToReq).subscribe();
  }
}
