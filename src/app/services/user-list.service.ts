import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { UserList } from '../model/user-list.model';
import { BehaviorSubject } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UserListService {
  public userListObs = new BehaviorSubject<Array<UserList>>([]);
  public userList;
  public allUserDish;
  userListForm: FormGroup;

  constructor(private http: HttpService)  {
    this.getUserList()

   }

   getUserList() {
    this.http.getUserList().subscribe(data => {
      this.userList = data;
    })

    this.userListObs.next(this.userList);
    return this.userListObs.asObservable();
   }

   getUserListBeId(id) {
    return this.http.getUserListById(id)
   }
  }
