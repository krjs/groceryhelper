import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  allUserList;
  userGroceryFinder;

  constructor(private http: HttpService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.getUserList();
  }

  getUserList() {
    this.http.getUserList().subscribe(data => {
      this.allUserList = this.groceryFinder(data)
    })

  }

  editList(id) {
    console.log(id + '<-=--sadsada')
    let userListToEdit = {
      id: id
    };
    this.router.navigate(['/add-list'], { queryParams: userListToEdit });
  }

  deleteList(id) {
    this.http.deleteUserList(id)

    this.allUserList = this.allUserList.filter(item => { return item.id !== id })
  }

  groceryFinder(arr) {
    return arr.map(item => {
      const dishName = item.userChoosedDish.map(item => item.dish),
            productValue = item.userChoosedDish.map(item => item.product).reduce((p, n) => [...p, ...n]),
            shopsCheck = productValue.map(item => {
                const arryOfshops = []
                if (!arryOfshops.includes(item.sklep)) {
                  arryOfshops.push(item.sklep)
                }
          return arryOfshops.reduce((p, n) => [...p, ...n])
        });

      const arrayWithoutDuplicateShops = shopsCheck.filter((object, index) => index === shopsCheck.findIndex(obj => JSON.stringify(obj) === JSON.stringify(object)));
      const arrayWithourDuplicateProductAndShops = productValue.filter((object, index) => index === productValue.findIndex(obj => JSON.stringify(obj) === JSON.stringify(object)));

      let listWithDish = {
        id: item._id.$oid,
        dishes: dishName,
        listName: item.listName,
        productAndShop: arrayWithourDuplicateProductAndShops,
        deleted: item.deleted,
        shops: arrayWithoutDuplicateShops
      }

      return listWithDish
    })
  }
}
