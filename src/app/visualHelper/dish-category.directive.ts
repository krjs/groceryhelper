import { Directive, ElementRef, Renderer2, OnInit, Input } from '@angular/core';

@Directive({
  selector: '[appDishCategory]'
})
export class DishCategoryDirective implements OnInit {

  constructor(private element:ElementRef, private renderer: Renderer2) { }
  @Input('appDishCategory') dishCategorz: string;

  ngOnInit():void{
    let li = this.element.nativeElement;
    if(this.dishCategorz === "Obiadowe") {
      this.renderer.setStyle(li, 'background-image', 'linear-gradient(to right, rgba(0,0,0,0), rgba(55,100,0,.5))')
    }

    if(this.dishCategorz === "Lekkie") {
      this.renderer.setStyle(li, 'background-image', 'linear-gradient(to right, rgba(0,0,0,0), rgba(248, 141, 0,.5))')
    }

    if(this.dishCategorz === "Desery") {
      this.renderer.setStyle(li, 'background-image', 'linear-gradient(to right, rgba(0,0,0,0), rgba(51, 145, 201,.5))')
    }

    if(this.dishCategorz === "Inne") {
      this.renderer.setStyle(li, 'background-image', 'linear-gradient(to right, rgba(0,0,0,0), rgba(254, 239, 49,.5))')
    }

  }
}
