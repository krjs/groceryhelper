import { Directive, ElementRef, Renderer2, OnInit, Input } from '@angular/core';

@Directive({
  selector: '[appShopsIcon]'
})
export class ShopsIconDirective implements OnInit {
  @Input('appShopsIcon') shop: string;

  constructor(private element:ElementRef, private renderer: Renderer2) { }

  ngOnInit():void {
    let li = this.element.nativeElement;
    this.renderer.setStyle(li, 'padding-left', '34px')

    if(this.shop === "Biedronka") {
      this.renderer.setStyle(li, 'background', 'url(/assets/biedronka2.png) no-repeat left top')
    }
    if(this.shop === "Lidl") {
      this.renderer.setStyle(li, 'background', 'url(/assets/lidl.png) no-repeat left top')
    }
    if(this.shop === "Zabka") {
      this.renderer.setStyle(li, 'background', 'url(/assets/zabka.png) no-repeat left top')
    }
    if(this.shop === "Inne") {
      this.renderer.setStyle(li, 'background', 'url(/assets/pytajnik.png) no-repeat left top')
    }
  }
}
