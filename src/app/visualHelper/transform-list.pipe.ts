import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'transformList'
})
export class TransformListPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    return value.charAt(0).toLocaleUpperCase() + value.slice(1);
  }

}
